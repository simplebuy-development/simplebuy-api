var debug = require('debug')('loopback:oauth2:jwt');
var oauth2 = require('loopback-component-oauth2');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var jwt = require('jws');
var uid = require('uid2');
var secret = require('../secret');


module.exports = function (app, options) {
  var loopback = app.loopback;
  var users = loopback.findModel(options.userModel) ||
    loopback.getModelByType(loopback.User);
  var oAuthTokenModel = loopback.findModel(options.tokenModel) ||
    loopback.findModel('OAuthAccessToken');
  var ttl = typeof options.getTTL === 'number' ? options.getTTL : 14 * 24 * 3600; // 2 weeks

  function userLogin(username, password, done) {
    debug('userLogin: %s', username);
    users.findOne({
      where: {
        username: username
      }
    }, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }
      user.hasPassword(password, function (err, matched) {
        if (err || !matched) {
          return done(err, false);
        }
        done(null, user);
      });
    });
  }

  function saveAccessToken(token, clientId, resourceOwner, scopes, refreshToken, done) {
    var tokenObj;
    if (arguments.length === 2 && typeof token === 'object') {
      tokenObj = token;
      done = clientId;
    }
    if (!tokenObj) {
      tokenObj = {
        id: token,
        appId: clientId,
        userId: resourceOwner,
        scopes: scopes,
        issuedAt: new Date(),
        expiresIn: ttl,
        refreshToken: refreshToken
      };
    }
    tokenObj.expiresIn = ttl;
    tokenObj.issuedAt = new Date();
    tokenObj.expiredAt = new Date(tokenObj.issuedAt.getTime() + ttl * 1000);
    oAuthTokenModel.create(tokenObj, done);
  }

  passport.use('loopback-oauth2-local-jwt', new LocalStrategy(userLogin));

  app.post(options.loginPath || '/login', function (req, res, next) {
      passport.authenticate('loopback-oauth2-local-jwt', {
        session: false,
        successReturnToOrRedirect: '/',
        failureRedirect: options.loginPage || '/login'
      }, function (err, user) {
        req.user = user;
        next();
      })(req, res, null)
    }, function (req, res, next) {
      oauth2.oauth2orize.exchange.password(
        function (client, username, password, scope, done) {
          var id = uid(32);
          var body = {
            header: {alg: options.alg || 'HS256'}, // Default to hash
            secret: secret,
            payload: {
              id: id,
              clientId: 'local',
              userId: req.user && req.user.id,
              username: req.user.username,
              scope: '*',
              createdAt: new Date()
            }
          };
          var token = jwt.sign(body);

          saveAccessToken(token, 'local', req.user.id, scope, null, function (err, accessToken) {
            done(null, accessToken, null, null); // send response with token
          })
        }
      )(req, res, next)
    }
  );
}
;
