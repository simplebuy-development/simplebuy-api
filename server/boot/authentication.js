var oauth2 = require('loopback-component-oauth2');

module.exports = function enableAuthentication(server) {
  // enable authentication
  // server.enableAuth();
  oauth2.authenticate(['/api'], {session: false, scope: 'all'});

};
